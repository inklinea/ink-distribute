#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Ink Distribute - Just an example of Exponent Horizontal Distribution
# Appears Under Extensions>Arrange
# An Inkscape 1.2+ extension
##############################################################################

# Update 251223 to test GitLab / GitKraken

import inkex
from inkex import Transform

def horiztonal_distribute(self, object_list, distribution_distance):

    number_of_objects = len(object_list)

    exp_list = []

    for count in range(1, number_of_objects):

        exponent_value = float(self.options.exponent_float)

        exp_list.append(count ** exponent_value)

    for item, exp_value in zip(object_list, exp_list):
        bbox = item.bounding_box()
        left = bbox.left

        fraction = exp_value / exp_list[-1]

        transform = Transform()
        transform.add_translate((fraction * distribution_distance))

        item.transform = item.transform @ transform


def x_zero(self, object_list):

    start_x = object_list[0].bounding_box().left
    end_x = object_list[-1].bounding_box().left
    distance = end_x - start_x

    for item in object_list:

        x = item.bounding_box().left
        x_shift = x - start_x

        x_translate = Transform()
        x_translate.add_translate(-x_shift)

        item.transform = item.transform @ x_translate

    return distance

class InkDistribute(inkex.EffectExtension):


    def add_arguments(self, pars):

        pars.add_argument("--exponent_float", type=float, dest="exponent_float", default=2)

    def effect(self):
        selection_list = self.svg.selected

        if len(selection_list) < 1:
            return

        distribution_distance = x_zero(self, selection_list)

        horiztonal_distribute(self, selection_list, distribution_distance)

if __name__ == '__main__':
    InkDistribute().run()
